package main

import (
	"fmt"
	"net/http"

	device "github.com/cli/oauth/device"
)

func main() {
	cid := "gitlab-client"
	scopes := []string{"repo", "read:org"}

	client := http.DefaultClient
	// Query the code endpoint to get device code.
	// modulate endpoints

	code_endpoint := "http://localhost:8090/auth/realms/Gitlab/protocol/openid-connect/auth/device"
	code, err := device.RequestCode(client, code_endpoint, cid, scopes)
	if err != nil {
		panic(err)
	}

	fmt.Printf("Open address %s\n in a brower on this or another device\n\n", code.VerificationURI)
	fmt.Printf("Enter code %s when prompted\n", code.UserCode)
	// "https://github.com/login/oauth/access_token"
	accessToken, err := device.PollToken(client, "http://localhost:8090/auth/realms/Gitlab/protocol/openid-connect/token", cid, code)

	// Need to handle this more gracefully when reporting to Gitlab
	if err != nil {
		panic(err)
	}

	// GIT_ASKPASS hooks this process'
	// stdout into git clone's stdin,
	// where it attempts to consume
	// a username and password
	// provide auth token as username
	// leave password blank, as per
	// https://github.blog/2012-09-21-easier-builds-and-deployments-using-git-over-https-and-oauth/
	fmt.Printf("%s", accessToken.Token)
}
